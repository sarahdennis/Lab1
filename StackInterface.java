/**
 * Interface for stacks of strings.
 * 
 * @author Sarah & David
 */
public interface StackInterface
{
    /**
     * Returns true if stack is empty, false otherwise.
     * @return whether stack is empty
     */
    boolean isEmpty();

    
    /**
     * Pushes specified string onto the top of the stack.
     * @param s the string to be pushed onto the stack
     */
    void push(String s);

    
    /**
     * Removes and returns string currently sitting on top of stack,
     * assuming stack is not empty. (If it is empty, it generates a
     * "stack underflow" error.)
     * @return string on top of stack that is removed
     */
    String pop();
    
}
