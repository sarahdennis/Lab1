import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class ParenMatch {

    public static void main(String[] args) throws IOException {
        // read contents of first argument into string
        String s = new String(Files.readAllBytes(Paths.get(args[0])));
        if (parensMatch(s)) {
            System.out.println("Balanced :)");
        } else {
            System.out.println("Imbalanced :(");
        }
    }

    public static boolean parensMatch(String s) {
        StackInterface stack = new Stack(); 
        boolean matches = false;
        String dummy;
        for (int i = 0; i < s.length(); i++){
            if (s.charAt(i) == '('){
                stack.push("x");
            } else if (s.charAt(i) == ')'){
                if (!stack.isEmpty()){
                    dummy = stack.pop();
                } else {
                    return false;
                }
            } 
        }
        if (stack.isEmpty()){
            matches = true;
        }
        return matches;
    }

}
