/**
 * Stacks of strings using lists (using resizable arrays).
 * 
 * @author Sarah & David
 */
public class Stack implements StackInterface {
    private StringListInterface _list;
    
    public Stack(){
        _list = new StringList();
    }
    
    public boolean isEmpty(){
        return _list.isEmpty();
    }
    
    public void push(String s){
        _list.add(s);
    }
    
    public String pop(){
        if(_list.isEmpty()){
            throw new RuntimeException("Nothing to return, empty stack");
        }else{
            int last = _list.size()-1;
            return _list.remove(last);
        }
    }
}
